import { useEffect, useState } from "react";

const useFetch = (url: any) => {
    const [data, setData] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null)

    
    useEffect(() => {
        fetch(url)
            .then(res => {
                if(!res.ok) {
                    throw Error('Failed to fetch data')
                }
                return res.json();
            })
            .then((data) => {
                setData(data)
                setIsLoading(false);
                setError(null);
            })
            .catch((e) => {
                setError(e.message)
                setIsLoading(false)
            })
    }, [url]);

    return { data, isLoading, error}
}

export default useFetch