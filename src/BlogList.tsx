import { Key, ReactChild, ReactFragment, ReactPortal } from "react";

const BlogList = (props: { blogs: any; }) => {
  const blogs = props.blogs
  return (
    <div className="blog-list">
      {blogs.map((blog: { id: Key | null | undefined; title: boolean | ReactChild | ReactFragment | ReactPortal | null | undefined; author: boolean | ReactChild | ReactFragment | ReactPortal | null | undefined; }) => (
        <div className="blog-preview" key={blog.id}>
          <h2>{blog.title} </h2>
          <p>Written by {blog.author}</p>
        </div>
      ))}
    </div>
  );
};

export default BlogList;
